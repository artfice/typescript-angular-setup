export interface ILocalStorageService {
    save<T>(key:string, value:T): void;
    load<T>(key:string): T;
    clear(): void;
}
