// import services
import LocalStorageService from './LocalStorageService/LocalStorageService';

// register services
export default function registerServices(appName:string):void {
    'use strict';

    angular.module(appName)
        .service('LocalStorageService', LocalStorageService);
}
