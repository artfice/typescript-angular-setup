///<reference path="../typings/tsd.d.ts"/>
import * as angular from 'angular';
import registerServices from './services/commonServiceModule';
import registerDirectives from './components/commonModule';

export const appName:string = 'common';

angular.module(appName, []);
registerServices(appName);
registerDirectives(appName);
