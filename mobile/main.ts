///<reference path="../typings/tsd.d.ts"/>
import * as angular from 'angular';
import registerDirectives from './component/ComponentModule';
import registerServices from './service/ServiceModule';

// app name
export const appName:string = 'mobile';

// register module, directives, services, etc.
angular.module(appName, ['common']).run(() => {
    console.log('App Starts');
});
registerDirectives(appName);
registerServices(appName);

// bootstrap Angular
let appAngularConfig:angular.IAngularBootstrapConfig = {
    debugInfoEnabled: true,
    strictDi: true
};
angular.bootstrap(
    document.getElementById('mobile'),
    [appName],
    appAngularConfig
);
