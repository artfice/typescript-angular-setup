import {IDataService} from './IDataService';
export default class DataService implements IDataService {
    private $window:angular.IWindowService;

    constructor($window:angular.IWindowService) {
        this.$window = $window;
    }

    public say(): void {
        console.log('hi');
    }
}

DataService.$inject = ['$window'];
