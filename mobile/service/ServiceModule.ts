// import directives
import DataService from './DataService/DataService';
import PluginRegistry from './PluginRegistry/PluginRegistry';

// register directives
export default function registerServices(appName:string):void {
    'use strict';

    angular.module(appName).service('DataService', DataService);
    angular.module(appName).service('PluginRegistryService', PluginRegistry);
}
