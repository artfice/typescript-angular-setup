export default class AbstractPlugin {
    protected _type:string;
    protected _provider:any;
    protected _cardinality:number;
    protected _contract:any;

    constructor(type: string, provider: any, cardinality: number, contract: any) {
        this._type = name;
        this._provider = provider;
        this._cardinality = cardinality;
        this._contract = contract;
    }

    protected validPlugin(): boolean {
        throw new Error('Not implemented');
    }
}
