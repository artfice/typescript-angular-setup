export default class StyleMap {

    public static map: any = {
        top: 't',
        bottom: 'b',
        left: 'l',
        right: 'r',
        padding: 'p',
        margin: 'm',
        na: '',
        buildStyle: function(type: string, direction: string, size: number): string {
            return this[type] + this[direction] + '-' + size;
        }
    };
}
