import AbstractComponent  from './AbstractComponent';
import StyleMap from '../StyleMap';
export default class Container extends AbstractComponent {

    constructor () {
        super('div');
    }

    public setPadding(size: number, direction: string): void {
        this.addClass(StyleMap.map.buildStyle('padding', direction, size));
    }

    public setMargin(size: number, direction: string): void {
        this.addClass(StyleMap.map.buildStyle('margin', direction, size));
    }

    public setInlineBlock (): void {
        this.addClass('inline-block');
    }
}
