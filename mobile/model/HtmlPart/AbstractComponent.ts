export default class AbstractComponent {
    protected _class: string[];
    protected _id: string;
    protected _attr: string[];
    protected _element: string;
    protected _children: AbstractComponent[];

    constructor (elementName: string) {
        this._element = elementName;
        this._class = [];
        this._id = '';
        this._attr = [];
        this._children = [];
    }

    public addClass (className: string): void {
        this._class.push(className);
    }

    public addAttribute (attr: string): void {
        this._attr.push(attr);
    }

    public removeClass (className: string): void {
        if (this._class.indexOf(className) > -1) {
            delete this._class[className];
        }
    }

    public removeAttribute (attr: string): void {
        if (this._attr.indexOf(attr) > -1) {
            delete this._attr[attr];
        }
    }

    public setId (id: string): void {
        this._id = id;
    }

    public addChild (element: AbstractComponent): void {
        this._children.push(element);
    }

    public build (): string {
        return '<'  + this._element +
                      this._buildClass() +
                      this._buildAttributes() + '>' +
                      this.buildChildren() +
               '</' + this._element + '>';
    }

    protected buildChildren (): string {
        let children: string = '',
            len: number = this._children.length;
        for (var i: number = 0; i < len; i++) {
            children += this._children[i].build();
        }
        return children;
    }

    private _buildClass (): string {
        let classPath: string = ' ';
        if (this._class.length > 0) {
            classPath = 'class="';
            classPath += this._class.join(' ');
            classPath += '"';
        }

        return classPath;
    };

    private _buildAttributes (): string {
        return ' ' + this._attr.join(' ');
    }

    private _buildId (): string {
        return (this._id.length > 0) ? this._id : ' ';
    }

}
