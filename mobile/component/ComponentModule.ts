// import directives
import TextNavBar from './navbar/text/TextNavBar';

// register directives
export default function registerDirectives(appName:string):void {
    'use strict';

    angular.module(appName).directive('textNavBar', TextNavBar);
}
