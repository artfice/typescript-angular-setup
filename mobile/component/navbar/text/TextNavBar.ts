import { html } from './textNavBar.html';

function TextNavBar(): angular.IDirective {
    'use strict';

    return {
        scope: {},
        template: html,
        controllerAs: 'vm',
        controller: [ () => {}],
        bindToController: {
        title: '='
        }
    };
}

export default TextNavBar;
