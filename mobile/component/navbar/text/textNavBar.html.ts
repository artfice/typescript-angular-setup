export const html:string = `
    <div class="navbar navbar-app navbar-absolute-top">
      <div class="navbar-brand navbar-brand-center font-raleway">
        {{title}}
      </div>
    </div>
`;
