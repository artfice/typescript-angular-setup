
var gulp = require('gulp');
var ts = require('gulp-typescript');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var del = require('del');
var merge = require('merge2');
var sourcemaps = require('gulp-sourcemaps');
var path = require('path');
var watch = require('gulp-watch');
var karma = require('karma').Server;
var tslint = require('gulp-tslint');


gulp.task('default', function () {
        return browserify({
            entries: 'main.js',
            debug: true
        })
            .bundle()
            .on('error', function (e) {
                console.log(e.toString());
                this.emit('end');
            })
            // save it as "{{applicationName}}Bundle.js"
            .pipe(source(path.join('vendor.js')))
            // in the application's "compiled" folder
            .pipe(gulp.dest(path.join('../', 'compiled')));
    });